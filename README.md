# Описати своїми словами навіщо потрібні функції у програмуванні.

- Для виконання заданих в функції дій, це можуть бути розрахунки, запуск якоїсь події на сторінці чи у застосунку, відправки чи отримання якоїсь інформації тощо.

# Описати своїми словами, навіщо у функцію передавати аргумент.

- Аргумент необхідний як значення з яким функція буде вести свій розрахунок, або запустить тий чи інший ланцюжок подій, тобто результат функції залежить від аргументу який в неї було передано. 

# Що таке оператор return та як він працює всередині функції? 

- Оператор return - як випливає з його назви "повертає" результат роботи функції. Якщо йому було надано якесь значення, він видасть його як результат роботи функції, й негайно завершить її виконання. Якщо значення задано не було, то функція поверне undefined.